<?php
    $url_host = 'http://'.$_SERVER['HTTP_HOST'];
    $pattern_document_root = addcslashes(realpath($_SERVER['DOCUMENT_ROOT']), '\\');
    $pattern_uri = '/' . $pattern_document_root . '(.*)$/';
    
    preg_match_all($pattern_uri, __DIR__, $matches);
    $url_path = $url_host . $matches[1][0];
    $url_path = str_replace('\\', '/', $url_path);
?>   


<div class="type-13">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        
                        <!--admin-panel-->
                        <div class="admin-panel">
                            <div class="admin-title">
                                <h3>
                            
                                    <small>Custom design</small>
                                </h3>
                                <ul class="items">
                                    <li>
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-wrench"></i>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Settings 1</a></li>
                                            <li><a href="#">Settings 2</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a class="close-link">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            
                            <!--admin-content-->
                            <div class="admin-content">
                                <p>Add class <code>bulk_action</code> to table for bulk actions options on row select</p>
                                <div class="info-table table-responsive">
                                    <table class="table data-table">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <input type="checkbox" class="checkbox" onclick="checkAll('checkbox', this)" >
                                                </th>
                                                <th>Invoice</th>
                                                <th>Invoice Date</th>
                                                <th>Order</th>
                                                <th>Bill to Name</th>
                                                <th>Status</th>
                                                <th>Amount</th>
                                                <th class="last"><span>Action</span>
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" class="checkbox">
                                                </td>
                                                <td>121000040</td>
                                                <td>May 23, 2014 11:47:56 PM </td>
                                                <td>121000210 <i class="success fa fa-long-arrow-up"></i></td>
                                                <td>John Blank L</td>
                                                <td>Paid</td>
                                                <td>$7.45</td>
                                                <td class="last"><a href="#">View</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" class="checkbox">
                                                </td>
                                                <td>121000037</td>
                                                <td>May 26, 2014 10:52:44 PM</td>
                                                <td>121000204</td>
                                                <td>Mike Smith</td>
                                                <td>Paid</td>
                                                <td>$333.21</td>
                                                <td class="last"><a href="#">View</a>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <input type="checkbox" class="checkbox">
                                                </td>
                                                <td>121000040</td>
                                                <td>May 27, 2014 11:47:56 PM </td>
                                                <td>121000210</td>
                                                <td>John Blank L</td>
                                                <td>Paid</td>
                                                <td>$7.45</td>
                                                <td class="last"><a href="#">View</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" class="checkbox">
                                                </td>
                                                <td>121000039</td>
                                                <td>May 28, 2014 11:30:12 PM</td>
                                                <td>121000208</td>
                                                <td>John Blank L</td>
                                                <td>Paid</td>
                                                <td>$741.20</td>
                                                <td class="last"><a href="#">View</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>